#include "wx/wx.h"
#include "wx/sizer.h"
#include <string>
#include <regex>
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <array>
#include <thread>

class BasicDrawPane : public wxWindow
{
    
public:
    BasicDrawPane(wxWindow* parent);
    
    
    int panelWidth = 625;
    int panelHeight = 625;
    int width = 625;
    int height = 625;
    int xLeft = -1 * width / 2;
    int yTop = height / 2;
    bool tempBool = false;
    double yRelationInputToPane = 1.0 * height / panelHeight;
    std::string function = "x";
    void OnPaint(wxPaintEvent& evt);
    void render(wxDC& dc, int width);
    std::string exec(const char* cmd);
    void ToggleStatus()
    {
        width = 100;
        Refresh();
    };
    DECLARE_EVENT_TABLE()
};


class MyApp: public wxApp
{
    bool OnInit();
public:
    
};

class MainFrame: public wxFrame
{
    public:
    MainFrame( const wxString &title, const wxPoint &pos, const wxSize &size );
    int width = 1250;
    int height = 650;
    wxButton *drawButton;
    wxTextCtrl *MainEditBox;
    wxTextCtrl *xLeftEditBox;
    wxTextCtrl *yTopEditBox;
    wxTextCtrl *xRightEditBox;
    wxTextCtrl *yBottomEditBox;
    BasicDrawPane* pane;
    
    void paintGraph( wxCommandEvent& event );
    void OnSize( wxSizeEvent& event);
  
    DECLARE_EVENT_TABLE()
};

IMPLEMENT_APP(MyApp)

enum
{
    BUTTON_Hello = wxID_HIGHEST + 1,// declares an id which will be used to call our button
    TEXT_Main = wxID_HIGHEST + 2, // declares an id which will be used to call our textfield
    TEXT_X_Left = wxID_HIGHEST + 3, // declares an id which will be used to call our textfield
    TEXT_Y_Top = wxID_HIGHEST + 4, // declares an id which will be used to call our textfield
    TEXT_X_Right = wxID_HIGHEST + 5, // declares an id which will be used to call our textfield
    TEXT_Y_Bottom = wxID_HIGHEST + 6 // declares an id which will be used to call our textfield
};

MainFrame::MainFrame(const wxString &title, const wxPoint &pos, const wxSize
   &size): wxFrame((wxFrame*)NULL,  - 1, title, pos, size)
{
    // graph pane
    pane = new BasicDrawPane( this );
    drawButton = new wxButton(this, BUTTON_Hello, _T("Hello World"), wxDefaultPosition, wxDefaultSize, 0);
    
    // Input function
    // Initialize our text box with an id of TEXT_Main, and the label "Function"
    // Create the wxStaticText control
    wxStaticText* staticText = new wxStaticText(this, wxID_ANY, "function");
    MainEditBox = new wxTextCtrl(this, TEXT_Main, "x", wxDefaultPosition, wxDefaultSize,
        wxTE_MULTILINE | wxTE_RICH , wxDefaultValidator, wxTextCtrlNameStr);
    wxBoxSizer* functionInput = new wxBoxSizer(wxHORIZONTAL);
    functionInput->Add(MainEditBox, 1, wxEXPAND);
    functionInput->Add(staticText, 1, wxEXPAND);
    
    // Input xLeft Coordinate
    // Create the wxStaticText control
    wxStaticText* staticTextXLeft = new wxStaticText(this, wxID_ANY, "X Left");
    xLeftEditBox = new wxTextCtrl(this, TEXT_X_Left, std::to_string(pane->xLeft), wxDefaultPosition, wxDefaultSize,
        wxTE_MULTILINE | wxTE_RICH , wxDefaultValidator, wxTextCtrlNameStr);
    wxBoxSizer* xLeftInput = new wxBoxSizer(wxHORIZONTAL);
    xLeftInput->Add(xLeftEditBox, 1, wxEXPAND);
    xLeftInput->Add(staticTextXLeft, 1, wxEXPAND);
    
    // Input yTop coordinate
    // Create the wxStaticText control
    wxStaticText* staticTextYTop = new wxStaticText(this, wxID_ANY, "Y Top");
    yTopEditBox = new wxTextCtrl(this, TEXT_Y_Top, std::to_string(pane->yTop), wxDefaultPosition, wxDefaultSize,
        wxTE_MULTILINE | wxTE_RICH , wxDefaultValidator, wxTextCtrlNameStr);
    wxBoxSizer* yTopInput = new wxBoxSizer(wxHORIZONTAL);
    yTopInput->Add(yTopEditBox, 1, wxEXPAND);
    yTopInput->Add(staticTextYTop, 1, wxEXPAND);
    
    // Input xRight coordinate
    // Create the wxStaticText control
    wxStaticText* staticTextXRight = new wxStaticText(this, wxID_ANY, "X Right");
    xRightEditBox = new wxTextCtrl(this, TEXT_X_Right, std::to_string(pane->width + pane->xLeft), wxDefaultPosition, wxDefaultSize,
        wxTE_MULTILINE | wxTE_RICH , wxDefaultValidator, wxTextCtrlNameStr);
    wxBoxSizer* xRightInput = new wxBoxSizer(wxHORIZONTAL);
    xRightInput->Add(xRightEditBox, 1, wxEXPAND);
    xRightInput->Add(staticTextXRight, 1, wxEXPAND);
    
    // Input yBottom coordinate
    // Create the wxStaticText control
    wxStaticText* staticTextYBottom = new wxStaticText(this, wxID_ANY, "Y Bottom");
    yBottomEditBox = new wxTextCtrl(this, TEXT_Y_Bottom, std::to_string(-1 * (pane->height - pane->yTop)), wxDefaultPosition, wxDefaultSize,
        wxTE_MULTILINE | wxTE_RICH , wxDefaultValidator, wxTextCtrlNameStr);
    wxBoxSizer* yBottomInput = new wxBoxSizer(wxHORIZONTAL);
    yBottomInput->Add(yBottomEditBox, 1, wxEXPAND);
    yBottomInput->Add(staticTextYBottom, 1, wxEXPAND);
    
    // Set the panel together
    wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* inputSizer = new wxBoxSizer(wxVERTICAL);
    inputSizer->Add(xLeftInput, 1, wxEXPAND);
    inputSizer->Add(yTopInput, 1, wxEXPAND);
    inputSizer->Add(xRightInput, 1, wxEXPAND);
    inputSizer->Add(yBottomInput, 1, wxEXPAND);
    inputSizer->Add(drawButton, 1, wxEXPAND);
    inputSizer->Add(functionInput, 1, wxEXPAND);
    sizer->Add(pane, 1, wxEXPAND);
    sizer->Add(inputSizer, 1, wxEXPAND);
    this->SetSizer(sizer);
}

bool MyApp::OnInit()
{
    MainFrame *MainWin = new MainFrame(_T("Hello World!"), wxPoint(1, 20), wxSize(1250,650));
    MainWin->Show(TRUE);
    SetTopWindow(MainWin);
    return true;
}

BEGIN_EVENT_TABLE ( MainFrame, wxFrame )
EVT_BUTTON ( BUTTON_Hello, MainFrame::paintGraph )
EVT_SIZE(MainFrame::OnSize)
END_EVENT_TABLE()

BEGIN_EVENT_TABLE(BasicDrawPane, wxPanel)
EVT_PAINT(BasicDrawPane::OnPaint)
END_EVENT_TABLE()

BasicDrawPane::BasicDrawPane(wxWindow* parent)
: wxWindow(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxFULL_REPAINT_ON_RESIZE)
{
}

void MainFrame::paintGraph(wxCommandEvent& event)
{
    std::cout << "button pressed" << std::endl;
    pane->xLeft = atoi(xLeftEditBox->GetValue());
    pane->yTop = atoi(yTopEditBox->GetValue());
    pane->width = atoi(xRightEditBox->GetValue()) - atoi(xLeftEditBox->GetValue());
    pane->height = atoi(yTopEditBox->GetValue()) - atoi(yBottomEditBox->GetValue());
    pane->function = MainEditBox->GetValue();
    Refresh();
}

void MainFrame::OnSize(wxSizeEvent& event)
{
    event.Skip();
    wxSize changedSize = event.GetSize();
    pane->panelWidth = changedSize.GetWidth() / 2;
    pane->panelHeight = changedSize.GetHeight();
    Refresh();
     
    std::cout << "resizing: widht=" << changedSize.GetWidth() << ", height=" << changedSize.GetHeight() << std::endl;
}

void BasicDrawPane::OnPaint(wxPaintEvent& event){
    wxPaintDC dc(this);
    render(dc, this->width);
}

void BasicDrawPane::render(wxDC&  dc, int width)
{
        dc.DrawText(function, width, 60);
        
        // Relations x,y input to PaintPanel
        double yVPToInputValuesRelation = 1.0 * height / panelHeight;
        double xVPToInputValuesRelation = 1.0 * width  / panelWidth;
        
        // Coordinate system lines
        int xCoordinateLineValue = 1.0 * yTop / yVPToInputValuesRelation;
        // draw a rectangle
        dc.SetBrush(*wxBLUE_BRUSH); // blue filling
        dc.SetPen( wxPen( wxColor(0,0,0), 1 ) ); // 10-pixels-thick pink outline
        dc.DrawLine( 0, xCoordinateLineValue, panelWidth, xCoordinateLineValue);
        int yCoordinateLineValue = 1.0 * xLeft * -1.0 / xVPToInputValuesRelation;
        dc.DrawLine(yCoordinateLineValue, 0, yCoordinateLineValue, panelHeight);
        
        std::string result = "-1";
        std::string temp = "";
        // x-Step for f(x) calculation
        double xStep = xVPToInputValuesRelation;
        double initialX = 1.0 * xLeft;
        int i = 0;
        while(i < panelWidth) {
            // y as String after script evaluation
            double tempDouble = initialX + i * xStep;
            temp = std::to_string(tempDouble);
            temp = std::regex_replace(function, std::regex("x"), temp);
            result = "bc <<< \"scale=4; " + temp + "\"";
            result = exec(result.c_str());
            // y for painting
            double yRelationInputToPane = 1.0 * height / panelHeight;
            double dY = 1.0 * (panelHeight - (std::stod(result) / yRelationInputToPane) - (panelHeight - xCoordinateLineValue));
            int tempInt = (int) dY;
            dc.DrawLine(i,tempInt,i + 1,tempInt);
            result = "-1";
            temp = "";
            i++;
        }
}

std::string BasicDrawPane::exec(const char* cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}
